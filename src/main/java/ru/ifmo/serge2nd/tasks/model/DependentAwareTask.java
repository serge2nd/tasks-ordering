package ru.ifmo.serge2nd.tasks.model;

import java.util.List;

public interface DependentAwareTask<T extends DependentAwareTask<T>> {
    List<T> getDependentTasks();
}
