package ru.ifmo.serge2nd.tasks.model;

public interface CountableDepsTask {
    int getDepsCount();
    void setDepsCount(int depsCount);

    boolean resolveDepAndCheck();
    void resetResolvedDeps();
}
