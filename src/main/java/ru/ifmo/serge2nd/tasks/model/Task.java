package ru.ifmo.serge2nd.tasks.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Task implements DependentAwareTask<Task>, CountableDepsTask {
    private final int num;
    private final List<Task> dependentTasks = new ArrayList<>();

    private int depsCount;
    private int resolvedDepsCounter;

    public Task(int num) { this.num = num; }

    public int getNum() { return this.num; }

    @Override
    public List<Task> getDependentTasks() { return dependentTasks; }

    @Override
    public int getDepsCount() { return this.depsCount; }
    @Override
    public void setDepsCount(int depsCount) { this.depsCount = depsCount; }

    @Override
    public boolean resolveDepAndCheck() {
        int depsCount = this.getDepsCount();

        if (resolvedDepsCounter < depsCount)
            return ++resolvedDepsCounter == depsCount;

        return true;
    }

    @Override
    public void resetResolvedDeps() { this.resolvedDepsCounter = 0; }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (!(obj instanceof Task))
            return false;

        return COMPARATOR_BY_NUM.compare(this, (Task)obj) == 0;
    }

    @Override
    public int hashCode() { return this.getNum(); }

    @Override
    public String toString() { return ""+this.getNum(); }

    public static final Comparator<Task> COMPARATOR_BY_NUM = Comparator.comparingInt(Task::getNum);
}
