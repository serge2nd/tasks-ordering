package ru.ifmo.serge2nd.tasks.scheduling;

import ru.ifmo.serge2nd.tasks.model.Task;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;

class ArrayTaskSet implements TaskSet {
    private static final int TASKS_LIMIT = 10000000;

    private final Task[] tasks = new Task[TASKS_LIMIT];
    private int tasksCount;

    @Override
    public Task computeIfAbsent(int i, Function<Integer, ? extends Task> taskFactory) {
        if (tasks[i] == null)
            tasks[i] = taskFactory.apply(i);

        if (i > tasksCount - 1)
            tasksCount = i + 1;

        return tasks[i];
    }

    @Override
    public Collection<Task> get() { return Arrays.asList(tasks).subList(0, tasksCount); }

    @Override
    public int count() { return tasksCount; }
}