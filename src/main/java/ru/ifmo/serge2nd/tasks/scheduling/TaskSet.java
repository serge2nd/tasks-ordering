package ru.ifmo.serge2nd.tasks.scheduling;

import ru.ifmo.serge2nd.tasks.model.Task;

import java.util.Collection;
import java.util.function.Function;

interface TaskSet {
    Task computeIfAbsent(int i, Function<Integer, ? extends Task> producer);
    Collection<Task> get();
    int count();
}