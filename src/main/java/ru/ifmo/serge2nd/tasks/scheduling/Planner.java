package ru.ifmo.serge2nd.tasks.scheduling;

import ru.ifmo.serge2nd.tasks.model.Task;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toSet;

public class Planner {
    private final TaskOrdering<Task> taskOrdering;

    public Planner(TaskOrdering<Task> taskOrdering) { this.taskOrdering = taskOrdering; }

    public List<List<Task>> planTasks(String taskPairListPath) throws IOException {
        Collection<Task> tasks = extractTasks(taskPairListPath);

        Set<Task> initialTasks = tasks.stream()
                .filter(t -> t != null && t.getDepsCount() == 0)
                .collect(toSet());

        List<List<Task>> result = new ArrayList<>();
        while (!initialTasks.isEmpty()) {
            result.add(new ArrayList<>(initialTasks));
            initialTasks = taskOrdering.nextIndependent(initialTasks);
        }

        return result;
    }

    private static Collection<Task> extractTasks(String taskPairListPath) throws IOException {
        TaskSet tasks = new ArrayTaskSet();

        Files.lines(Paths.get(taskPairListPath)).forEach(new Consumer<String>() {
            private int lineNum = 0;

            public void accept(String line) {
                ++lineNum;

                if (!line.isEmpty()) {
                    String[] parts = line.split(" ");

                    try {
                        if (parts.length != 2)
                            throw new ArrayIndexOutOfBoundsException("the line must contain two values separated by a single space!");
                        int i = Integer.parseInt(parts[0]);
                        int j = Integer.parseInt(parts[1]);

                        Task dependency = tasks.computeIfAbsent(i, Task::new);
                        Task dependent = tasks.computeIfAbsent(j, Task::new);

                        dependency.getDependentTasks().add(dependent);
                        dependent.setDepsCount(dependent.getDepsCount() + 1);
                    } catch(Exception e) {
                        throw new IllegalArgumentException("error parsing the line " + lineNum + " (see below)", e);
                    }
                }
            }
        });

        return tasks.get();
    }
}
