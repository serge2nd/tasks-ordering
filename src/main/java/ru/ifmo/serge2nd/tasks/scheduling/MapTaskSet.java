package ru.ifmo.serge2nd.tasks.scheduling;

import ru.ifmo.serge2nd.tasks.model.Task;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

class MapTaskSet<T extends Map<Integer, Task>> implements TaskSet {
    private final T tasks;

    public MapTaskSet(Class<? extends T> tasksMapClass) throws IllegalAccessException, InstantiationException {
        this.tasks = tasksMapClass.newInstance();
    }

    @Override
    public Task computeIfAbsent(int i, Function<Integer, ? extends Task> taskProducer) {
        return tasks.computeIfAbsent(i, taskProducer);
    }

    @Override
    public Collection<Task> get() { return tasks.values(); }

    @Override
    public int count() { return tasks.size(); }
}