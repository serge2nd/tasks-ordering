package ru.ifmo.serge2nd.tasks.scheduling;

import ru.ifmo.serge2nd.tasks.model.CountableDepsTask;
import ru.ifmo.serge2nd.tasks.model.DependentAwareTask;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class TaskOrderingImpl<T extends DependentAwareTask<T> & CountableDepsTask> implements TaskOrdering<T> {
    private final Comparator<? super T> taskComparator;

    public TaskOrderingImpl(Comparator<? super T> taskComparator) { this.taskComparator = taskComparator; }

    @Override
    public Set<T> nextIndependent(Set<T> independentTasks) {
        Set<T> nextTaskGroup = new TreeSet<>(taskComparator);

        independentTasks.forEach(task -> task.getDependentTasks()
            .forEach(child -> {
                if (child.resolveDepAndCheck())
                    nextTaskGroup.add(child);
            })
        );

        return nextTaskGroup;
    }

    @Override
    public Comparator<? super T> taskComparator() { return this.taskComparator; }
}
