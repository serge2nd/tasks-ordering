package ru.ifmo.serge2nd.tasks.scheduling;

import java.util.Comparator;
import java.util.Set;

public interface TaskOrdering<T> {
    Set<T> nextIndependent(Set<T> independentTasks);
    Comparator<? super T> taskComparator();
}
