package ru.ifmo.serge2nd.tasks.scheduling;

import org.junit.Test;
import ru.ifmo.serge2nd.tasks.model.Task;

import java.io.IOException;

public class PlannerTest {
    private final Planner planner = new Planner(new TaskOrderingImpl<>(Task.COMPARATOR_BY_NUM));

    @Test
    public void testSample() throws IOException {
        System.out.println(planner.planTasks("target/classes/sample.in"));
    }

    @Test
    public void testHugeSample() throws IOException {
        System.out.println(planner.planTasks("target/classes/huge-sample.in").size());
    }
}